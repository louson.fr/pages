AUTHOR = 'Louis Rannou'
SITENAME = 'LOUSON'
SITEURL = "https://louson.fr"

PATH = "content"

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
    ("Gitlab", "https://gitlab.com/Louson"),
    ("Github", "https://github.com/Louson"),
    ("Archlinux", "https://wiki.archlinux.org/title/User:Louson"),
)

# Social widget
SOCIAL = (
    ("Linkedin", "https://www.linkedin.com/in/louis-rannou-90606931/"),
    ("Mastodon", "https://framapiaf.org/deck/@Louson"),
)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

# Custom config
THEME = "themes/pelican-bootstrap3"
BOOTSTRAP_THEME = 'sandstone'
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}
PLUGINS = [
    'plugins.i18n_subsites',
    'pelican.plugins.pandoc_reader',
    'pelican.plugins.sitemap',
]

# bootstram theme config
PYGMENTS_STYLE = 'zenburn'
ABOUT_ME = \
    ''' Linux embedded developer and Yocto consultant.  Check out my <a
    href=/pages/cv.html>CV</a> '''
CC_LICENSE = 'CC-BY-SA'

SITEMAP = {
    'format': 'txt',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

DISPLAY_PAGES_ON_MENU = False
DISPLAY_CATEGORIES_ON_MENU = False
MENUITEMS = [
    (' Papers ', '/category/papers.html'),
    ('  Blog  ', '/category/blog.html'),
    # (' Readings ', '/category/readings.html'),
    ('Who am I', '/pages/louis-rannou.html'),
]
