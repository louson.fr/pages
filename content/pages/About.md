---
title: Louis Rannou
date: '2024-08-16'
---

I am a Linux embedded developer and Yocto consultant, check my
[linkedin](https://www.linkedin.com/in/louis-rannou-90606931/) or download my
[CV]({static}/pdfs/CV_Louis_Rannou.pdf).

I share some code on [Gitlab](https://gitlab.com/Louson)

I am an Archlinux user, look at [my archwiki
page](https://wiki.archlinux.org/title/User:Louson)

## Stunning books

### 2023

* **Book** Calculating stars by Marie Robinette Kowal (Vers les Étoiles)
* **Book** A Prayer for the Crown-Shy (Une prière pour les cimes timides)
* **Book** A Closed and Common Orbit by Becky Chambers (Libration)

### 2022

* **Book** A Psalm for the Wild-Built by Becky Chambers (Un psaume pour les
  recyclés sauvages)
* **Book** The Long Way to a Small, Angry Planet by Becky Chambers (L'Espace
  d'un an)

* **Comic** *FR* Majnoun et Leili by Yann Damezin
* **Comic** *FR* Mon ami Pierrot by Jim Bishop
* **Comic** The Hunting Accident: A True Story of Crime and Poetry by David
  L. Carlson and Landis Blair (L'accident de chasse)

### 2021

* **Comic** On a sunbeam by Tillie Walden (Dans un rayon de soleil)

### 2020

* **Book** The Inheritance trilogy by N. K. Jemisin (Chroniques de la Terre fracturée)
* **Book** The Earthsea cycle by Ursula Le Guin (Les contes de Terremer)

### 2018

* **Manga** Pluto by Naoki Urasawa

### 2017

* **Book** *FR* La horde du contrevent by Alain Damasio (The WindWalkers)

### 2016

* **Comic** *FR*  Goupil ou Face by Lou Lubie
* **Comic** *FR* Le mystère du monde quantique by Mathieu Buriat and Thibaut Damour

### Before

* **Comic** *FR* Un Océan d'amour by Wilfrid Lupano and Grégory Panaccione

### Some time ago

* **Book** Spin by Robert Charles Winston

### A long time ago

* **Comic** Mafalda by Quino
* **Manga** One Piece by Eichiro Oda
* **Manga** Dragon Ball by Akira Toriyama
* **Manga** Slam Dunk by Takehiko Inoue

