---
title: CV
date: '2024-09-10'
---

My CV is made with [moderncv](), you can [download
it]({static}/pdfs/CV_Louis_Rannou.pdf) or compile on your own:

```tex
\documentclass[
  a4paper,
]{moderncv}

\moderncvtheme[
  green
]{classic}

\usepackage[utf8]{inputenc}
\usepackage[scale=0.8]{geometry}
\recomputelengths

\firstname{Louis}
\lastname{Rannou}

\title{Embedded Linux Developer}
\quote{
  Excellent knowledge of the Linux ecosystem \\
  Enthusiast C developer/architect and bug hunter \\
  Makes the link between hardware teams and cloud/edge \\
  Good socialization, willing to share and learn, team player}
\email{louis.rannou@syslinbit.com}
\address{}{Grenoble or remote}{}
\mobile{+33777069509}
\extrainfo{\httplink{https://gitlab.com/Louson-public}}

\nopagenumbers{}

\begin{document}
\maketitle

\section{Skills}

\cvcomputer{Languages}{\textbf{C}, C++, Python}{Systems}{GNU/Linux, uboot, systemd}

\cvcomputer{Toolchains}{\textbf{Yocto Project}, buildroot, crosstool}{OTA updates}{SWUpdate, RAUC,
  Memfault}

\cvcomputer{Tools}{git, gcc, clang, cmake, meson}{CI}{Jenkins, Jira, GitHub,
  Gitlab}

\cvcomputer{Tests}{Unit, functional, integration \\(C, C++,
  Python)}{Architecture}{Oriented Object Programming, UML, documentation}

\section{Experience}

\cventry{Since 2023}{Yocto consulting}{Schneider Electric}{Grenoble,
  France}{}{Yocto consulting for an advanced Linux distribution supposed to
  become the reference design for Schneider devices with a stron gfocus on
  testing and security. Support for reference boards including Linux
  and U-boot configuration.}

\cventry{Since 2023}{Linux Trainer}{Le campus numérique}{Grenoble,
  France}{}{Training on Linux for DevOps: algo, containers, programming}

\cventry{2023}{Yocto developer}{Yocto Project}{}{}{Work on SPDX3 support for
  SBOMs}

\cventry{2021-2023}{Linux, Yocto\&OTA
  consultant}{Baylibre}{France}{}{YoctoProject debug, Linux support and OTA
  exploration with SWUpdate, RAUC and Memfault}

\cventry{since 2021}{Founder}{Syslinbit}{France}{}{Founded a consultancy company
  uniting freelancers focused on Open Source and Embedded Systems. Syslinbits
  offers services at low level, cloud/edge communication and security.}

\cventry{2020--2021}{Linux developer}{Enlaps}{France}{}{BSP setup for imx7 low
  power, SPI debug, boot sequence and Linux support with OTA on SWUpdate}

\subsection{Embedded Linux Developer, Open Wide \& Smile (2015--2019)}

\cventry{2019--2020}{Yocto integrator and Linux
  consultant}{Eaton}{France}{}{Work on the update of the former Linux
  buildsystem and replacement of outdated core software such as OTA updates and
  Rest API for PDU (Power Distribution Unit)
    \begin{itemize}
    \item
      SW support for HW teams
    \item
      integration of an external memory as an overlay
    \item
      integration of HTTP server with TLS 1.3 support and development of
      theFastCGI layer to offer a Rest API.
    \item
      OTA updates with SWUpdate
    \end{itemize}
}

\cventry{2018--2019}{Linux and OTA consultant}{Capsys}{France}{}{Architecture
  and development of an embedded gateway to manage the communication between
  public transports and trafic lights. Development of an OTA solution with
  \textbf{SWUpdate}}

\cventry{2017--2018}{IoT Linux developer}{}{France}{}{GSM integration to secure a
  gateway connection with automatic switch from ethernet to GSM.}

\cventry{2016--2017}{IoT lead developer}{Smile}{France}{}{Architecture and
  development of an \textbf{IoT} prototype for the \textbf{SunTripTour} 2017~:
  \begin{itemize}
  \item
    Yocto/Linux for Raspberry Pi with touchscreen
  \item
    Catching data from various sensors (GPS, speed, heart beat, pressure,
    temperature...)
  \item
    Support for several communications (BLE, Garmin/Ant, Wifi, Sigfox, Lora,
    3G/4G)
  \end{itemize}
}

\cventry{2016--2019}{Linux C developer}{See}{France}{}{Development of
  communication stack for the eDMR protocol
  \begin{itemize}
  \item
    complex decoding driver with efficiency and security requirements
  \item
    communication kernel/userspace through netdev sockets
  \item
    high level decoding and forward to an Asterix server
  \end{itemize}
}

\cventry{2015--2016}{Linux C developer}{See}{France}{}{Linux\&Yocto setup for a core
  system embedded in tramways}

\cventry{2015}{Linux consultant}{Sagem/Airbus}{France}{}{Support on a complex
  and sensitive logger system for Airbus planes}

\subsection{Linux C/C++ developer}

\cventry{2012-2014}{Linux C developer}{Soft At Home}{France}{}{Work on a C
  middleware for TV set-top-box with a strong focus on testing (unit,
  integration and functional). Support for DTT and DTS, conception of a complex
  resource manager, support for the central firmware manager, support for
  networking}

\cventry{2011-2012}{Intern}{Archos}{France}{}{Conception of a remote controller
  with camera detection and OpenCV for Android devices}

\section{Volunteer work}

\cventry{Volunteer}{Rézine}{Grenoble}{France}{}{Help for the development of a
    neutral network and internet provider \httplink{rezine.org} through radio
    and fiber. Support for Wireguard and OpenWRT, help on the management and
    presentations to the public (schools...)}

\cventry{Developper}{}{}{}{}{Work on a
  \href{https://pypi.org/project/mbzero/}{mbzero}, python bindings for the
  \httplink{musicbrainz.org} API to replace the outdated musicbrainzngs}

\cvline{Author (french)}{GPG management
  \httplink{http://www.linuxembedded.fr/2017/09/initiation-a-gnu-privacy-guard/}
}

\cvline{Author (french)}{Toolchains with crosstool-ng
  \httplink{linuxembedded.fr/2018/09/crosstool-ng-human-crossing/}
}

\cvline{Author (french)}{Database with rrdtool
  \httplink{https://gitlab.com/Louson/rrdtool\_presentation}
}

\section{Education}

\cventry{2008-2011}{MSc}{ENSIMAG}{France}{}{C, C++, SQL databases, electronic}
\cventry{2011}{Exchange year}{UFRGS}{Brasil}{}{Concurrent computing, security and
cryptography}

\section{Additional informations}

\cvline{Interests}{Open Source, Science-fiction (with a strong focus on
  women authors), low techs, mathematics, French comics}

\cvline{Keywords}{C C++ Python Unit tests Functional tests Integration tests
  Linux kernel u-boot IoT buildroot Yocto Oriented object UML Microcontroller
  Security git GitHub ARM Systemd D-bus dm-verity dm-crypt}

\cvline{Soft skills}{Curious, Good quality, Teamwork, Share}

\end{document}
```
