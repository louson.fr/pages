---
title: Looking for a static site generators
summary: A hugo deception
data: '2024-09-11'
status: draft
---

Hugo is not an easy framework for building a static website.

Hugo is a framework of frameworks. Each theme has its own specific configuration
and metadatas. Pages you write for a theme won't work for an other theme.

Community themes are old and unmaintained.

CSS hack is required.

<!-- I was looking for a static site generator. I know some html - my first -->
<!-- website was done with foundation - but I prefer to write in markdown as the -->
<!-- source can be read and it's easiert to version with git. My second website was -->
<!-- done with Grav. It was great but it wasn't easy to maintain and it went down for -->
<!-- quite a long time. -->

<!-- - markdown writing -->
<!-- - optional live edit -->
<!-- - no CSS to write -->
<!-- - maintained -->
<!-- - git synchronization -->
<!-- - available maintained themes -->
<!-- - portability: I can change theme or framework -->
<!-- - all FOSS -->
<!-- - no js -->

For the third time, I had a look at Hugo. 


It's easy to set up and having my
pages online. Then I looked for a theme as the default one was not really
convincing. There are plenty on <https://themes.gohugo.io/>. Actually a lot of
them is old and unmaintained. All require specific configs and metadatas. Most
would require to change the theme.

Each theme is actually a Hugo framework and in my opinion that makes a poor
community. Perhaps we should see the themes as a portofolio, and Hugo as a
commercial place where to find someone to develop your website. I'm fine with
that but Hugo is not the right framework for setting up a website without
technical knowledge.

