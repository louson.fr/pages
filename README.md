# Pages

Static data and configuration for [louson.fr](https://louson.fr)

## Pelican CMS

The website can be generated using [pelican](https://getpelican.com).

## Requirements

- pelican
- pelican-pandoc-reader

## Generation

Generate with `pelican content`, or make the website up with `pelican --listen`.
